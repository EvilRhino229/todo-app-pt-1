// import { Switch, Route } from "react-router-dom";
import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList.js";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  
  handleToggleComplete = id => () => {
    const newTodos = this.state.todos.map(todo => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed
        };
      }
      
      return todo;
    });
    this.setState({ todos: newTodos });
  };
  
  handleDelete = id => () => {
    const newTodos = this.state.todos.filter(todo => todo.id !== id);
    this.setState({ todos: newTodos });
  };

  handleDeleteCompleted = () => {
    const newTodos = this.state.todos.filter(todo => todo.completed === false);
    this.setState({
      todos: newTodos
    });
  };
  
  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleCreate = event => {
    if (event.key === "Enter") {
      const newTodos = this.state.todos.slice();
      newTodos.push({
        userId: 1,
        id: Math.ceil(Math.random() * 100),
        title: this.state.value,
        completed: false
      });
      this.setState({
        value: "",
        todos: newTodos
      });
    }
  };
  
  render() {
    return (
      <section className="Todos">
        <header className="header">
          <h1>Todos</h1>
          <input
            className="new-item" placeholder="What needs to be done?" autoFocus onChange={this.handleChange} onKeyDown={this.handleCreate} value={this.state.value}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleToggleComplete={this.handleToggleComplete}
        />
        {/* <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button
            className="clear-completed" onClick={this.handleDeleteCompleted}>Clear completed</button>
        </footer> */}
        <footer className="footer">
        <span className="todo-count">
        <strong>{this.state.todos.length}</strong> item(s) left
        </span>
        {/* <ul className="filters">
          <li>
            <a href="/">All</a>
          </li>
          <li>
            <a href="/active">Active</a>
          </li>
          <li>
            <a href="/completed">Completed</a>
          </li>
        </ul> */}
        <button className="clear-completed" onClick={this.handleDeleteCompleted}>Clear completed</button>
      </footer>
      </section>
    );
  }
}



export default App;
